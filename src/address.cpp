#include <iostream>

#include "memtrace.h"
#include "address.h"


using namespace std;

string_view Address::getZip() const { return zip; }
void Address::setZip(string_view newZip) { zip = newZip; }

string_view Address::getCity() const { return city; }
void Address::setCity(string_view newCity) { city = newCity; }

string_view Address::getStreet() const { return street; }
void Address::setStreet(string_view newStreet) { street = newStreet; }

string_view Address::getHouseNumber() const { return houseNumber; }
void Address::setHouseNumber(string_view newHouseNumber) { this->houseNumber = newHouseNumber; }

ostream& operator<<(ostream& os, const Address& address)  {
    os << address.getZip() << " " << address.getStreet() << " " << address.getHouseNumber() << ", " << address.getCity();
    return os;
}

istream& operator>>(istream& in, Address& address) {
    cout << "Enter zip code: ";
    in >> address.zip;
    cout << "Enter street name: ";
    in >> address.street;
    cout << "Enter house number: ";
    in >> address.houseNumber;
    cout << "Enter city: ";
    in >> address.city;
    return in;
}
