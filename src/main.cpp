#ifndef CPORTA

#include <iostream>
#include "memtrace.h"
#include "phonebook.h"
#include "contact.h"
#include <string>



/**
 * @brief This is the command menu for the phonebook.
 * It lists all the available commands.
 * 
 */
constexpr std::string_view  MENU("\n1) Add Contact\n2) Edit Contact\n3) Remove Contact\n\n4) List Phonebook\n\n5) Import Phonebook form file\n6) Save Phonebook to file\n\n7) Search for a contact\n\n8) Exit program\n\n");

using namespace std;

/**
 * @brief Enumerator for the commands.
 * 
 */
enum class Command {
    INVALID = -1, ADD, EDIT, REMOVE, LIST, IMPORT, SAVE, SEARCH, EXIT
};

/**
 * @brief This method excecutes a given command on the phonebook. 
 * Sets the isRunning varriable if necesary to exit the main loop.
 * 
 * @param cmd The command to be executed.
 * @param pb The phonebook to excecute the command on.
 * @param isRunning The boolean of the main controll loop.
 */
void executeCmd(const Command& cmd, PhoneBook* pb, bool* isRunning);

/**
 * @brief This method validates a given raw command and if it valid it tourns into a enum command.
 * 
 * @return Command The command from the input in the rigth format.
 */
Command parseCmd();

/**
 * @brief Validates if a raw input could be a command.
 * 
 * @return true if its a possible command.
 * @return false otherwise
 */
bool isRawCmdValid(string_view);

/**
 * @brief This method turns a stirng to an enum command.
 * 
 * @return Command This is the result of the conversion form a given string.
 */
Command fromRawToEnumCmd(const string&);

/**
 * @brief Gets a raw command form the standard input.
 * 
 */
void getRawCmd(string*);

/**
 * @brief Validates if a given string is a valid file name, if its a .txt file.
 * 
 * @return true the format is correct.
 * @return false otherwise.
 */
bool isValidFile(string_view fileName);

/**
 * @brief Checks if the phonebook is empty or not.
 * 
 * @return true if the phonebook is empty.
 * @return false otherwise.
 */
bool isPbEmpty(const PhoneBook*);

/**
 * @brief Calls the edit method of tha phonebook. With the paramters from the standard input.
 * 
 */
void editContact(PhoneBook*);

/**
 * @brief Calls the add method of the phonebook. With the paramters form the standard input.
 * 
 */
void addContact(PhoneBook*);

/**
 * @brief Calls the remove method of the phonebook. With the paramters form the standard input.
 * 
 */
void removeContact(PhoneBook*);

/**
 * @brief Calls the get method of the phonebook to show a given contact. With the paramters form the standard input method.
 * 
 */
void searchPhoneBook(const PhoneBook*);

/**
 * @brief Gets the filename for the standrad input, validates the extension of the file name
 * and calls the import method of the phonebook.
 * 
 */
void importPhoneBook(PhoneBook*);

/**
 * @brief Get a fileName form the standard input, validates the extension of the file name
 * it and calls the export method of the phonebook.
 * 
 */
void exportPhoneBook(const PhoneBook*);

/**
 * @brief Prints the menu to the standard output.
 * 
 */
void printCmdMenu();

/**
 * @brief Exits the program garcefully by clearing out the phonebook.
 * 
 */
void shutDown(PhoneBook*, bool*);

/**
 * @brief Main controll method of the program. Runns until the user chooses otherwise.
 * Otherwise it stays in an infinitle loop and printing th emenu and executes the given command form the standard input.
 * 
 * @return int 
 */
int main() {
    auto pb = new PhoneBook();
    bool isRunning = true;
    while(isRunning) {
        printCmdMenu();
        executeCmd(parseCmd(), pb, &isRunning);
    }
    return 0;
}



void executeCmd(const Command& cmd, PhoneBook* pb, bool* isRunning) {
    switch (cmd) {
        case Command::ADD:
            addContact(pb);
            break;
        case Command::EDIT:
            if (isPbEmpty(pb)) {
                cout << "PhoneBook is empty!\n";
            } else {
                editContact(pb);
            }
            break;
        case Command::REMOVE:
            if (isPbEmpty(pb)) {
                cout << "PhoneBook is empty!\n";
            } else {
                removeContact(pb);
            }
            break;
        case Command::LIST:
            if (isPbEmpty(pb)) {
                cout << "PhoneBook is empty!\n";
            } else {
                cout << pb;
            }
            break;
        case Command::IMPORT:
            if (!isPbEmpty(pb)) {
                cout << "PhoneBook is not empty, cannot import!\n";
            } else {
                importPhoneBook(pb);
            }
            break;
        case Command::SAVE:
            if (isPbEmpty(pb)) {
                cout << "PhoneBook is empty!\n";
            } else {
                exportPhoneBook(pb);
            }
            break;
        case Command::SEARCH:
            if (isPbEmpty(pb)) {
                cout << "PhoneBook is empty!\n";
            } else {
                searchPhoneBook(pb);
            }
            break;
        case Command::EXIT:
            shutDown(pb, isRunning);
            break;
        default:
            cout << "Invalid command, it must be a number (1-7)\n";
    }
}

Command parseCmd() {
    string rawCmd;
    getRawCmd(&rawCmd);
    if (!isRawCmdValid(rawCmd)) return Command::INVALID;
    return fromRawToEnumCmd(rawCmd);
}

void getRawCmd(string* s) {
    cin >> *s;
}

bool isRawCmdValid(string_view rawCmd) {
    for (auto it = rawCmd.begin(); it != rawCmd.end(); ++it) {
        if (!isdigit(*it)) return false;
    }
    return true;
}

Command fromRawToEnumCmd(const string& rawCmd) {
    Command res;
    switch (stoi(rawCmd)) {
        case 1:
            res = Command::ADD;
            break;
        case 2:
            res = Command::EDIT;
            break;
        case 3:
            res = Command::REMOVE;
            break;
        case 4:
            res = Command::LIST;
            break;
        case 5:
            res = Command::IMPORT;
            break;
        case 6:
            res = Command::SAVE;
            break;
        case 7:
            res = Command::SEARCH;
            break;
        case 8:
            res = Command::EXIT;
            break;
        default:
            res = Command::INVALID;
    }
    return res;
}

void editContact(PhoneBook* pb) {
    string oldMobile;
    cout << "Enter mobile to edit a contact:\n";
    cin >> oldMobile;
    const Contact* oldContact = pb->getContact(oldMobile);
    if (oldContact == nullptr) {
        cout << "Contact doesn't exist!\n";
        return;
    }
    cout << "Contact to be edited:\n";
    cout << *oldContact;
    pb->removeContact(oldMobile);

    Contact newContact;
    cin >> newContact;
    pb->addContact(newContact);
}

void addContact(PhoneBook* pb) {
    Contact temp;
    cin >> temp;
    pb->addContact(temp);
}

void searchPhoneBook(const PhoneBook* pb) {
    string mobile;
    cout << "Enter mobile to search a contact:\n";
    cin >> mobile;
    const Contact* temp = pb->getContact(mobile);
    if ( temp == nullptr) {
        cout << "Contact doesn't exist!\n";
        return;
    }
    cout << *temp;
}

bool isPbEmpty(const PhoneBook* pb) {
    return pb->getSize() == 0;
}

void removeContact(PhoneBook* pb) {
    string temp;
    cout << "Enter mobile to remove contact:\n";
    cin >> temp;
    if (pb->getContact(temp) == nullptr) {
        cout << "Contact doesn't exist!\n";
        return;
    }
    pb->removeContact(temp);
    if (pb->getContact(temp) == nullptr) {
        cout << "Contact has been removed!\n";
    } else {
        cout << "Something went wrong during contact removal.\n";
    }
}

void importPhoneBook(PhoneBook* pb) {
    string fileName;
    cout << "Enter file name:\n";
    cin >> fileName;
    if (!isValidFile(fileName)) return;
    try {
        pb->importPhoneBook(fileName);
        cout << "Contacts from file have been imported!\n";
    } catch (const out_of_range&) {
        cout << "\n\n\nERROR: File content is invalid!\n\n        (×_×#)\n\n";
        delete pb;
        pb = new PhoneBook();
    }
}

void exportPhoneBook(const PhoneBook* pb) {
    string fileName;
    cout << "Enter file name:\n";
    cin >> fileName;
    if (!isValidFile(fileName)) return;
    pb->exportPhoneBook(fileName);
    cout << "PhoneBook has been successfully exported! to " << fileName << "!\n";
}

bool isValidFile(string_view fileName) {
    if (fileName.length() <= 4 || fileName.substr(fileName.length() - 4).compare(".txt") != 0) {
        cout << "Wrong file extension!\n";
        return false;
    }
    return true;
}
void printCmdMenu() {
    cout << MENU;
}

void shutDown(PhoneBook* pb, bool* isRunning) {
    delete pb;
    *isRunning = false;
}

#endif