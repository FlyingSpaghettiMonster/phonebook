
#include "memtrace.h"
#include "phonebook.h"

using namespace std;

size_t PhoneBook::getSize() const { return size; }

void PhoneBook::addContact(const Contact& contact) {
    auto temp = new Contact[size + 1];
    for (size_t i = 0; i < size; ++i) {
        temp[i] = list[i];
    }
    temp[size] = contact;

    delete[] list;
    size++;

    list = temp;
}

void PhoneBook::removeContact(std::string_view mobile) {
    int idx = -1;
    for (size_t i = 0; i < size; ++i) {
        if (list[i].getMobile() == mobile) {
            idx = static_cast<int>(i);
            break;
        }
    }

    if (idx == -1) {
        return;
    }

    auto temp = new Contact[size - 1];
    size_t tempIdx = 0;
    
    for (size_t i = 0; i < size; ++i) {
        if (idx == static_cast<int>(i)) {
            continue;
        } else {
            temp[tempIdx++] = list[i];
        }
    }

    delete[] list;
    size--;

    list = temp;    
}

Contact* PhoneBook::getContact(std::string_view mobile) const {
    for (size_t i = 0; i < size; ++i) {
        if (list[i].getMobile() == mobile) {
            return &list[i];
        }
    }
    return nullptr;
}

void PhoneBook::editContact(const Contact& oldContact, const Contact& newContact) const {
    *PhoneBook::getContact(oldContact.getMobile()) = newContact;
}

void PhoneBook::importPhoneBook(const string& fileName) {
    ifstream fin;
    fin.open(fileName);
    if (!fin.is_open()) {
        cout << "Could not open file!\n";
        return;
    }
    string line;
    while (fin) {
        getline(fin, line);
        if (line.compare("") == 0) { break; }
        array<string, 8> arr;
        stringstream ssin(line);
        for (int i = 0; i < 8 && ssin.good(); ++i) {
            ssin >> arr[i];
        }
        string fName = arr[0];
        string lName = arr[1];
        string nName = arr[2].substr(1, arr[2].length() - 3); //(), format!

        string zip = arr[3];
        string street = arr[4];
        string houseNumber = arr[5].substr(0, arr[5].length() - 1); //, format!
        string city = arr[6];

        string mobile = arr[7];

        auto tempAdr = Address(zip, street, houseNumber, city);
        auto tempc = Contact(fName, lName, nName, tempAdr, mobile);
        addContact(tempc);
    }
    fin.close();
}

void PhoneBook::exportPhoneBook(const std::string& fileName) const {
    ofstream fout;
    fout.open(fileName);
    if (!fout.is_open()) {
        cout << "Could not open file!\n";
        return;
    }
    fout << this;
    fout.close();
}

void PhoneBook::clear() {
    delete[] list;
    list = nullptr;
    size = 0;
}

PhoneBook::~PhoneBook() {
    clear();
}

ostream& operator<<(std::ostream& os, const PhoneBook* pb) {
    for (size_t i = 0; i < pb->getSize(); i++) {
        os << pb->list[i];
    }
    return os;
}