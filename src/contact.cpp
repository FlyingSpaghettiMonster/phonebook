

#include "memtrace.h"
#include "contact.h"

using namespace std;

string Contact::getFirstName() const { return firstName; }
void Contact::setFirstName(string_view newFirstName) { firstName = newFirstName; }

string Contact::getLastName() const { return lastName; }
void Contact::setLastName(string_view newLastName) { lastName = newLastName; }

string Contact::getNickName() const { return nickName; }
void Contact::setNickName(string_view newNickName) { nickName = newNickName; }

Address Contact::getAddress() const { return address; }
void Contact::setAddress(const Address& newAddress) { address = newAddress; }

string Contact::getMobile() const { return mobile; }
void Contact::setMobile(string_view newMobi) { mobile = newMobi; }

ostream& operator<<(ostream& os, const Contact& con) {
    os << con.getFirstName() << " " << con.getLastName() << " (" << con.getNickName() << "), " << con.address << " " << con.getMobile() << "\n";
    return os;
}

istream& operator>>(istream& in, Contact& con) {
    cout << "Enter first name: ";
    in >> con.firstName;
    cout << "Enter last name: ";
    in >> con.lastName;
    cout << "Enter nick name: ";
    in >> con.nickName;
    in >> con.address;
    cout << "Enter mobile number: ";
    in >> con.mobile;
    return in;
}


