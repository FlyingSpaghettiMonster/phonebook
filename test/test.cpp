#ifdef CPORTA
#include "gtest_lite.h"
#include "phonebook.h"

int main() {
    auto adr = Address();
    auto contact = Contact();
    auto pb = PhoneBook();

    TEST(Address, Getters()/Setters()) {
        adr.setZip("zip");
        EXPECT_STREQ("zip", adr.getZip().data());
        adr.setStreet("street");
        EXPECT_STREQ("street", adr.getStreet().data());
        adr.setHouseNumber("houseNumber");
        EXPECT_STREQ("houseNumber", adr.getHouseNumber().data());
        adr.setCity("city");
        EXPECT_STREQ("city", adr.getCity().data());
    }
    END

    TEST(Contact, Getters()/Setters()) {
        contact.setFirstName("fName");
        EXPECT_STREQ("fName", contact.getFirstName().data());
        contact.setLastName("lName");
        EXPECT_STREQ("lName", contact.getLastName().data());
        contact.setNickName("nName");
        EXPECT_STREQ("nName", contact.getNickName().data());
        contact.setMobile("mobile");
        EXPECT_STREQ("mobile", contact.getMobile().data());
        contact.setAddress(adr);
        EXPECT_STREQ("zip", contact.getAddress().getZip().data());
    }
    END

    TEST(PhoneBook, Init) {
        EXPECT_EQ(0, pb.getSize());
    }
    END

    TEST(Phonebook, Import()) {
        pb.importPhoneBook("ValidPhoneBook.txt");
        EXPECT_EQ(6, pb.getSize());
    }
    END

    TEST(PhoneBook, Export()) {
        pb.exportPhoneBook("NewValidPhoneBook.txt");
        auto pb2 = PhoneBook();
        pb2.importPhoneBook("NewValidPhoneBook.txt");
        EXPECT_EQ(6, pb2.getSize());
    }
    END

    TEST(PhoneBook, Clear()) {
        pb.clear();
        EXPECT_TRUE(nullptr == pb.getContact(""));
        EXPECT_EQ(0, pb.getSize());
    }
    END

    TEST(PhoneBook, Add()) {
        pb = PhoneBook();
        pb.addContact(contact);
        EXPECT_EQ(1, pb.getSize());
    }
    END

    TEST(PhoneBook, Edit()) {
        auto newContact = Contact("","","",Address(),"mobile");
        pb.editContact(contact, newContact);
        EXPECT_EQ(1, pb.getSize());
        EXPECT_EQ(newContact.getMobile(), pb.getContact("mobile")->getMobile());
    }
    END

    TEST(PhoneBook, Remove()) {
        pb.removeContact("mobile");
        EXPECT_EQ(0, pb.getSize());
    }
    END
    return 0;
}

#endif // CPORTA
