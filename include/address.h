#ifndef ADDRESS_H
#define ADDRESS_H

#include <string>


/**
 * @brief This class represents a Address.
 * 
 * @details Every Contact has a Address, which stores the address information of the Contact, such as zip code, street name, house number and city,
 * where these attributes are std:string(s), to make them more convenient.
 */
class Address {
    std::string zip, street, houseNumber, city;
public:
    /**
     * @brief Construct a new Address object.
     * 
     * @param zip This parameter is the zip code of the Contact's address.
     * @param street This parameter is the street name of the Contact's address.
     * @param houseNumber This parameter is the house number of the Contact's address.
     * @param city This parameter is the city of the Contact's address.
     */
    Address(std::string_view zip = "", std::string_view street = "", std::string_view houseNumber = "", std::string_view city = "")
    : zip(zip), street(street), houseNumber(houseNumber), city(city) {};

    /**
     * @brief Construct a new Address object. This is the default copy constructor of the Address class.
     * 
     * @param adr The address to be copied.
     */
    Address(const Address& adr) = default;

    /**
     * @brief Getter for the zip code of the address.
     * 
     * @return std::string_view The zip code.
     */
    std::string_view getZip() const;

    /**
     * @brief Setter for the zip cod of the address.
     * 
     * @param zip The zip code to be set.
     */
    void setZip(const std::string_view zip);

    /**
     * @brief Getter for the house number of the address.
     * 
     * @return std::string_view The house number.
     */
    std::string_view getHouseNumber() const;

    /**
     * @brief Setter for the house number object of the address.
     * 
     * @param houseNumber The house number of the address.
     */
    void setHouseNumber( const std::string_view houseNumber);

    /**
     * @brief Geter for the city of the address.
     * 
     * @return std::string_view The name of the city of the address.
     */
    std::string_view  getCity() const;

    /**
     * @brief Setter for the city name of the address.
     * 
     * @param city The name of the city of the address.
     */
    void setCity(std::string_view cityName);

    /**
     * @brief Getter for the street name of the address.
     * 
     * @return std::string_view 
     */
    std::string_view  getStreet() const;

    /**
     * @brief Setter for the street name of the address.
     * 
     * @param streetName The name of the street of the address.
     */
    void setStreet(std::string_view streetName);

    /**
     * @brief Default initializator of the address.
     * 
     * @param adr The address to be initialized form.
     */
    Address& operator=(const Address& adr) = default;

    /**
     * @brief Overloading of the << operator to print to given stream.
     * 
     * @param os The current output stream.
     * @param adr This address object.
     * @return std::ostream& The stream in use.
     */
    friend std::ostream& operator<<(std::ostream& os, const Address& adr);

    /**
     * @brief Overloading of the >> operator to print to given stream.
     * 
     * @param os The current input stream.
     * @param adr This address object.
     * @return std::istream& The stream in use.
     */
    friend std::istream& operator>>(std::istream& os, Address& adr);


    /**
     * @brief Destroys the Address object
     * 
     */
    ~Address() = default;
};

#endif // ADDRESS_H