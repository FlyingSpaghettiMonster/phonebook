#ifndef CONTACT_H
#define CONTACT_H

#include "address.h"

/**
 * @brief This class represents a Contact.
 * 
 * @details The phonebook holds multiple objects of this class, which stores the details of a contact,
 * such as the name, phone number and address. All attributes are stored as strings to make them more convenient.
 */
class Contact {
    Address address;
    std::string firstName, lastName, nickName, mobile;
public:
    /**
     * @brief Construct a new Contact object.
     * 
     * @param firstName This parameter is the first name of the contact.
     * @param lastName  This parameter is the last name of the contact.
     * @param nickName This parameter is the nickname of the contact.
     * @param adr This parameter is the address of the contact.
     * @param mobile This parameter is the phone number of the contact.
     */
    Contact(const std::string& firstName = "", const std::string& lastName = "", const std::string& nickName = "", const Address& adr = Address(),  const std::string& mobile = "")
        :address(adr),firstName(firstName),lastName(lastName),nickName(nickName),mobile(mobile) {};

    /**
     * @brief Getter for the First Name of the contact.
     * 
     * @return std::string The first name.
     */
    std::string getFirstName() const;

    /**
     * @brief Setter for the First Name of the contact.
     * 
     * @param fName The frist name to be set.
     */
    void setFirstName(std::string_view fName);

    /**
     * @brief Getter for the Last Name of the contact.
     * 
     * @return std::string The last name of the contact.
     */
    std::string getLastName() const;

    /**
     * @brief Setter for the Last Name of the contact.
     * 
     * @param lastName The last name to be set.
     */
    void setLastName(std::string_view lName);

    /**
     * @brief Getter for the Nick Name of the contact.
     * 
     * @return std::string The last name.
     */
    std::string getNickName() const;

    /**
     * @brief Setter for the Nick Name of the contact.
     * 
     * @param nName The nickname to be set.
     */
    void setNickName(std::string_view nName);

    /**
     * @brief Getter for the Address of the contact.
     * 
     * @return Address The address of the contact.
     */
    Address getAddress() const;

    /**
     * @brief Setter for the Address of the contact.
     * 
     * @param adr The address to be set.
     */
    void setAddress(const Address& adr);

    /**
     * @brief Getter for the Mobile of the contact.
     * 
     * @return std::string The mobile number of the contact.
     */
    std::string getMobile() const;

    /**
     * @brief Setter for the Mobile of the contact.
     * 
     * @param mobile The mobile number to be set.
     */
    void setMobile(std::string_view mobile);

    /**
     * @brief Default initializator of the contact.
     * 
     * @return Contact& The contact to be initialized from.
     */
    Contact& operator=(const Contact&) = default;

    /**
     * @brief Overloading of the << operator to print to given stream.
     * 
     * @param os The current output stream.
     * @param cont This contact object.
     * @return std::ostream& The stream in use.
     */
    friend std::ostream& operator<<(std::ostream& os, const Contact& cont);

    /**
     * @brief Overloading of the >> operator to print to given stream.
     * 
     * @param os The current input stream.
     * @param cont This contact object.
     * @return std::istream& The stream in use.
     */
    friend std::istream& operator>>(std::istream& in, Contact& cont);

    /**
     * @brief Destroys the Address object
     * 
     */
    ~Contact() = default;
};

#endif // CONTACT_H