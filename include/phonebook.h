#ifndef PHONEBOOK_H
#define PHONEBOOK_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <array>
#include "contact.h"

/**
 * @brief This class represents the data of the phonebook.
 * 
 * @details This class holds all Contact in the same dynamic array.
 * It is the interface between the controll and the model. 
 * It stores all of the contact in the same collection.
 * 
 */
class PhoneBook {
    Contact *list;
    size_t size;
public:
    /**
     * @brief Construct a new Phone Book object
     * 
     * @param list This parameter is the list of contacts.
     * @param size This paramter is the size of the list.
     */
    PhoneBook(Contact *list = nullptr,  size_t size = 0) :list(list), size(size) {};

    /**
     * @brief Getter for the Size of the list.
     * 
     * @return size_t This is the size of the list.
     */
    size_t getSize() const;

    /**
     * @brief This method appends the list with a new contact.
     * it creates a copy of the old list with the new contact.
     * 
     * @param cont This is the new contact to be added.
     */
    void addContact(const Contact& cont);

    /**
     * @brief This method removes contact by mobile phone number.
     * It creates a copy of the old list without the given contact found by mobile.
     * If the given contact is not found it returns.
     * 
     * @param mobile This the mobile of the contact to be removed.
     */
    void removeContact(std::string_view);
    
    /**
     * @brief This method linearly seaches the list and looks for a given contact's mobile number.
     * 
     * @return Contact* This is the pointer to the contact in the list with the given phone number. It can be nullptr!
     */
    Contact* getContact(std::string_view mobile) const;

    /**
     * @brief This method edits a Contact's attribute
     * 
     * @param oldCont This is the old contact to be edited.
     * @param newCont This is the new contact to be set.
     */
    void editContact(const Contact& oldCont, const Contact& newCont) const;

    /**
     * @brief This method imports form a given file and if the format of the file as expected
     *  then copy it's data to the phonebook.
     * 
     * @param fileName This is the name of the file to be imported form.
     */
    void importPhoneBook(const std::string& fileName);

    /**
     * @brief This method exports the data of the phonebook to a given file.
     * If the phonebook is empty then it returns.
     * The given file, if exists, will be overwritten.
     * 
     * @param fileName This is the name of the file to be exported to.
     */
    void exportPhoneBook(const std::string& fileName) const;

    /**
     * @brief This method prints the data of the phonebook to a given stream.
     * 
     * @param os This is the output stream to be printed to.
     * @param pb This is the phonebook to be printed from.
     * @return std::ostream& The stream in use.
     */
    friend std::ostream& operator<<(std::ostream& os, const PhoneBook* pb);

    /**
     * @brief This method deletes all contact in the list. Frees memory and resetes the size of the phonebook to zero.
     * 
     */
    void clear();

    /**
     * @brief Destroy the Phone Book object by calling the Clear method.
     * 
     */
    ~PhoneBook();
};
#endif // PHONEBOOK_H