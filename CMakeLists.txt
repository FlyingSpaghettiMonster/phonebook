# cmake version to be used
cmake_minimum_required( VERSION 3.0 )

# project name
project( NHF LANGUAGES CXX)

# flags
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_compile_options(
    -Wall -Wextra
    #-Werror
    -ggdb3
    -DMEMTRACE
    # Vedd ki az eggyel lentebbi sorból kommentet a tesztek futtatásához!
    #-DCPORTA -DMEMTRACE
)

# include
include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

# Create target and set properties.
set(SOURCE_FILES
    ./src/address.cpp
    ./src/contact.cpp
    ./src/phonebook.cpp
    ./src/main.cpp
    ./src/memtrace.cpp
    ./test/test.cpp
)

add_executable( NHF ${SOURCE_FILES} )
